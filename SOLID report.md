# SOLID programing principles

Solid is an acronym in which each letter has specific meaning to it, each letter conveys a particular quality of code that developers should preserve while writing code in an object oriented environment, in order to make the codebase much more readable , durable and faster.

S-O-L-I-D breaks down to the following , we will discuss each of the following properties in detail.
  - S - Single responsibility principle
  - O - Open closed principle 
  - L - Liskov Substitution principle
  - I - Interface segregation principle
  - D - Dependency inversion principle

### Single responsibility principle 
As the name suggests this rule says that a class should be assigned one and only one unique responsibility. This is done so as to ensure that any changes made to this class never effects the functionality of that class.

```sh
class Shape():
    def __init__(self,height,width):
        self.height = height
        self.width  = width 
    def area(self):
        return self.height * self.width 
    def perimeter(self):
        return 2 * (self.height + self.width)
```
In the above code we can see that the shape class has multiple responsibilities , this is not acceptable according to the single responsibility principle.

If we are following single responsibility principle then , we should implement each functionality in a different class
```sh
class Shape():
    def __init__(self,height,width):
        self.height = height
        self.width = width 
class Area():
    def area(self,height,width):
        return self.height * self.width
```

### Open closed principle
The open close principle demands that classes should be open only to extend to sub classes and should be closed for any direct modification.

So here we are having a class ice cream where if the ice cream type is chocolate we give extra five  points to  that ice cream .So if we implement it directly then our code looks like this.

```sh
class Ice_cream():
    def __init__(self,type,name):
        self.name = name
        self.type = type
        self.point = 1
    def classification(self):
        if self.name == "Cone":
            self.point += 3
        if self.type == "Chocolate":
            self.point += 5

```
But when we take account of the open closed Principle, we should implement it the following way , by creating another class and extending the parent.
```sh
class Ice_cream():
    def __init__(self,type,name):
        self.name = name
        self.type = type
        self.point = 1
    def classification(self):
        if self.type == "Cone":
            self.point += 3

class Chocolate(Ice_cream):
    def is_chocolate(self):
        if self.type == "chocolate":
            super().self.point += 5

```
### Liskov substitution principle 
This principle says that objects of superclasses should be replaceable with objects of its subclasses and they will be interrelated with each other, and they would exhibit mostly the same property. In other words we could say ,for any class, a client should be able to use any of its subtypes indistinguishably,without affecting output behaviour at runtime.




### Interface segregation principle
This principle says that the client side interfaces should be built in such a way that they are very specific to the task that they are designed to do. clients should be very feasible with it.This principle deals with the disadvantages of implementing big interfaces.Instead of going for big interface its better to provide small interfaces which provides better readability and functionality.

### Dependency inversion principle
This principle says that high-level modules should not depend on  low-level modules and they must remain mutually exclusive.Both low and high level modules should depend on the same abstraction that lay between them. In other words we can say that one should depend on abstractions and not on concrete instances.

In the below code we can see that instead of adding the tyre class  functionality directly on to the vehicle class , we used a sperate class to distinguish it and avoid any dependency that tend to have with the vehicle class.
```sh
class Tyre():
    def __init__(self,details):
        self.details = {}
class Vehicle():
    def __init__(self,name):
        self.name = name
    def speed(self):
        pass
class Bike(Vehicle,Tyre):
    def __init__(self):
        pass
class Car(Vehicl,Tyre):
    def __init__(self):
        pass

```



##### References:
- https://www.researchgate.net/publication/323935872_SOLID_Python_SOLID_principles_applied_to_a_dynamic_programming_language
- https://medium.com/@dorela/s-o-l-i-d-principles-explained-in-python-with-examples-3332520b90ff
- https://www.youtube.com/watch?v=yxf2spbpTSw&ab_channel=in28minutesCloud%2CDevOpsandMicroservices
